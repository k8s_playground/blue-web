Thi is a basic HTML web project only showing coloured page. The Dockerfile builds nginx server. The Gitlab CI Pipeline builds the server app and pushes it to GitLab Container Registry where a Kubernetes cluster is supposed to take it and deploy it.

## Build Docker image
```
# Built it for local use
docker build -t blueweb:latest .
# Built it for CI pipeline
docker build -t $CI_REGISTRY_IMAGE/blueweb:latest .
```

## Push Docker image to container registry

```
# Docker Hub
docker push audiblebits/blueweb:latest
# GitLab Container Registry
docker push $CI_REGISTRY_IMAGE/blueweb:latest
```

The Docker container serves the static `index.html` page via Nginx on port 80.

## Run Docker image
Run the Docker image by publishing the port 80 within the container on port 8080 within your host machine. 

```
docker run --rm --name blueweb -p 8080:80 -d blueweb:latest
docker run --rm --name blueweb -p 8080:80 -d audiblebits/blueweb:latest

```

Now you can open web browser at http://localhost:8080

